import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
#%matplotlib inline
import seaborn as sns

# kickstarter201801_clean_url='https://gitlab.com/group25/data-analysis-for-kickstarter-projects/raw/master/dataset/ks-projects-201801-Cleaning.csv'
# ks18=pd.read_csv(kickstarter201801_clean_url)


def categoryPlot(ks18):
    # Main Category
    # Pledged amount across categories
    pledged_sum_cate={}
    for category in list(set(ks18['main_category'])):
        pledged_sum_cate[category]=ks18[ks18['main_category']==category]['usd_pledged_real'].sum()
    # create dataframe   
    categ=pd.Series(pledged_sum_cate)
    categ=pd.DataFrame(categ)
    categ=categ.rename(columns={0:"pledged_sum"})

    # Number of projects across categories
    cate_count={}
    for category in list(set(ks18['main_category'])):
        cate_count[category]=ks18[ks18['main_category']==category]['main_category'].count()

    categ['count']=pd.Series(cate_count)

    # Success rate across categories
    success={}
    for category in list(set(ks18['main_category'])):
        success[category]=len(ks18[(ks18['main_category']==category) & (ks18['state']=="successful")])
        
    categ['success_count']=pd.Series(success)
    categ['Success']=categ['success_count']/categ['count']

    # Backers across categories
    backers={}
    for category in list(set(ks18['main_category'])):
        backers[category]=ks18[ks18['main_category']==category]['backers'].sum()
        
    categ['backers']=pd.Series(backers)

    # pledged_sum plot
    categ=categ.sort_values('pledged_sum', ascending=False)
    plt.figure(figsize=(6,6))
    pledg=categ['pledged_sum'][0:-3]
    add={}
    add['Others']=categ['pledged_sum'][-3:].sum()
    pledg=pledg.append(pd.Series(add))
    patches,l_text,p_text=plt.pie(pledg,labels=pledg.index,autopct='%3.1f%%',startangle=0,pctdistance=0.8)
    for t in l_text:
        t.set_fontsize(15)
    for t in p_text:
        t.set_fontsize(13)
    plt.axis('equal')
    plt.title('Pledged Money',fontsize=17)
    plt.show()

    # count plot
    categ=categ.sort_values('count', ascending=False)
    plt.figure(figsize=(6,6))
    count=categ['count']
    patches,l_text,p_text=plt.pie(count,labels=count.index,autopct='%3.1f%%',startangle=0,pctdistance=0.7)
    for t in l_text:
        t.set_fontsize(15)
    for t in p_text:
        t.set_fontsize(13)
    plt.axis('equal')
    #plt.title('Number of Projects',fontsize=17)
    plt.show()

    # success_rate plot
    import matplotlib.ticker as mtick
    categ=categ.sort_values('Success', ascending=True)
    suc=categ['Success']
    suc=pd.DataFrame(suc)
    suc=suc.rename(columns={0:"Success"})
    suc['Failure']=1-suc['Success']
    suc.plot.barh(stacked=True,width = 0.85,figsize=(12,6),color=['mediumturquoise','salmon'])
    plt.gca().xaxis.set_major_formatter(mtick.PercentFormatter(xmax=1))
    #plt.grid(c='white')
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.legend(bbox_to_anchor=(1,1),loc='best',fontsize=16)
    plt.title('Success vs. Failure Rate by Project Category',fontsize=18)
    plt.show()

    # backers plot
    categ=categ.sort_values('backers', ascending=False)
    plt.figure(figsize=(6,6))
    backers=categ['backers'][0:-3]
    add={}
    add['Others']=categ['backers'][-3:].sum()
    backers=backers.append(pd.Series(add))
    patches,l_text,p_text=plt.pie(backers,labels=backers.index,autopct='%3.1f%%',startangle=0,pctdistance=0.7)
    for t in l_text:
        t.set_fontsize(15)
    for t in p_text:
        t.set_fontsize(13)
    plt.axis('equal')
    plt.title('Number of Backers',fontsize=17)
    plt.show()

    # usd_goal_real
    goal_sum={}
    for category in list(set(ks18['main_category'])):
        goal_sum[category]=ks18[ks18['main_category']==category]['usd_goal_real'].sum()


    categ['goal_sum']=pd.Series(goal_sum)
    categ['goal_average']=categ['goal_sum']/categ['count']
    categ=categ.sort_values('goal_average', ascending=False)
    plt.subplots(figsize=(20,5))
    sns.set_style("whitegrid")
    sns.barplot(categ['goal_average'].index, y=categ['goal_average'], palette="Set1", saturation=0.5)
    sns.despine(right=True, top=True)
    plt.title('Average Goal per Project',fontsize=20)
    plt.xticks([])
    #plt.xticks(rotation=25)
    plt.yticks(fontsize=16)
    plt.ylabel('Dollars',fontsize=16)
    plt.show()

    goal_success={}
    for category in list(set(ks18['main_category'])):
        goal_success[category]=ks18[(ks18['main_category']==category) & (ks18['state']=="successful")]['usd_goal_real'].sum()
    categ['goal_average_success']=pd.Series(goal_success)/categ['count']

    plt.subplots(figsize=(20,5))
    sns.set_style("whitegrid")
    sns.barplot(categ['goal_average_success'].index, y=categ['goal_average_success'], palette="Set1", saturation=0.5)
    sns.despine(right=True, top=True)
    plt.xticks(fontsize=16)
    plt.xticks(rotation=25)
    plt.yticks(fontsize=16)
    plt.ylabel('Dollars',fontsize=16)
    plt.title('Average Goal per Successful Project',fontsize=20)
    plt.show()

    Rate_of_return={}
    for category in list(set(ks18['main_category'])):
        index=(ks18['main_category']==category) & (ks18['state']=="successful")
        Rate_of_return[category]=np.mean(ks18[index]['usd_pledged_real']/ks18[index]['usd_goal_real'])
    categ['Rate_of_return']=pd.Series(Rate_of_return)

    categ=categ.sort_values('Rate_of_return', ascending=False)
    plt.subplots(figsize=(12,5))
    sns.set_style("whitegrid")
    sns.barplot(categ['Rate_of_return'].index, y=categ['Rate_of_return'], palette="Set1", saturation=0.5)
    sns.despine(right=True, top=True)
    plt.xticks(fontsize=16)
    plt.xticks(rotation=35)
    plt.yticks(fontsize=16)
    plt.ylabel('')
    plt.title('Ratio of Return per Successful Project',fontsize=20)
    plt.show()
