
# coding: utf-8

# In[1]:


#data cleaning (Python’s Pandas and NumPy libraries)
#import data
#total data amount = 375764
import numpy as np
import pandas as pd
kickstarter201801_url='https://gitlab.com/group25/data-analysis-for-kickstarter-projects/raw/master/dataset/ks-projects-201801.csv'
ks_18=pd.read_csv(kickstarter201801_url)


# In[2]:


#first check if the ID of subjects are unique
#Using 'ID' as the index of the DataFrame
if ks_18['ID'].is_unique:
    ks_18.set_index('ID',inplace=True)


# In[3]:


#ID: don't need
#name: not necessary
#category & main category: project types
#currency & country: kickstarter users distribution
#launched and deadline: raise money time and duration
#goal amount: need to be converted to uniform currency (i.e. USD)
#pledged amount: need to be converted to uniform currency (i.e. USD)
#Q: usd_pledged vs usd_pledged_real
#number of backers

#state(results): failed & successful & others
#mainly determined by comparing goal amount & pledged amount

#Renaming columns to a more recognizable set of labels
#Adding necessary columns

#change to datetime & compute duration
ks_18['deadline'] = pd.to_datetime(ks_18['deadline'])
ks_18['launched'] = pd.to_datetime(ks_18['launched']).dt.date # get launched date without exact time on that day
ks_18['launched'] = pd.to_datetime(ks_18['launched'])
ks_18['duration'] = ks_18['deadline']-ks_18['launched'] # add duration column


# In[4]:


#Dropping unnecessary columns in a DataFrame    
ks_18.drop('name',axis=1,inplace=True)
ks_18.drop('goal',axis=1,inplace=True)
ks_18.drop('pledged',axis=1,inplace=True)
ks_18.drop('currency',axis=1,inplace=True)
ks_18.drop('usd pledged',axis=1,inplace=True)

#reindexing
ks_18 = ks_18.reindex(columns=['category','main_category','country','launched','deadline',
                             'duration','usd_pledged_real','usd_goal_real','backers','state'])


# In[5]:


# check missing values
missing_values_count=ks_18.isnull().sum()
print(missing_values_count)
print(ks_18.dtypes)


# In[6]:


ks_18.info()


# In[7]:


# Extract launched years
ks_18['launched year']=pd.to_datetime(ks_18['launched']).dt.year
ks_18.head()


# In[8]:


grp_year=ks_18.groupby('launched year')
grp_year.count()


# In[9]:


grp_year.get_group(1970)


# In[10]:


# drop invalid data whose launched year is 1970
ks_18.drop(1014746686, inplace=True)
ks_18.drop(1245461087, inplace=True)
ks_18.drop(1384087152, inplace=True)
ks_18.drop(1480763647, inplace=True)
ks_18.drop(330942060, inplace=True)
ks_18.drop(462917959, inplace=True)
ks_18.drop(69489148, inplace=True)

