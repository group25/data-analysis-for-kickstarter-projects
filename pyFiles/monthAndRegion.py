import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import os
import matplotlib
import matplotlib.pyplot as plt
from IPython.display import SVG, display
import pygal
# import seaborn as sns
# %matplotlib inline

# def initialize():
url = './dataset/ks-projects-201801-Cleaning.csv'
with open(url , 'r') as f:
    ksData = pd.read_csv(f)#.fillna(0)
def initialize(ksData):
    # adding ylunched year and month to dataset
    ksData.drop(['launched year'], axis = 1, inplace = True)
    ksData['launched_year'] = pd.DatetimeIndex(ksData['launched']).year
    ksData['launched_month'] = pd.DatetimeIndex(ksData['launched']).month
# ksData.head()
    # print(a)
    #2871668

def monthlyplot(ksData):
    # plot for number of projects and successful 
    ## need to use sort_index to plot the value
    projectNumMonly = ksData['launched_month'].value_counts().rename('Number of projects').sort_index()
    successfulNumMonthly = ksData[ksData['state'] == 'successful']['launched_month'].value_counts().rename('Number of successful projects').sort_index()
    successfulRate = (successfulNumMonthly/projectNumMonly).rename('Projects successful rate')
    backerNumMonthly = ksData.groupby('launched_month')['backers'].sum()/150
    backerNumMonthly = backerNumMonthly.rename('backers / 150 (scaled)')
    monthPlot = pd.concat([projectNumMonly, successfulNumMonthly,backerNumMonthly], axis=1, join='inner')

    pledgedMonthly = ksData.groupby('launched_month')['usd_pledged_real'].sum()

    # fig, axes = plt.subplots(nrows=4, ncols=1)
    # monthPlot.plot(ax=axes[0],figsize=(15, 15), legend=True, title='Total Projects and Successful Projects')
    plt.figure();
    monthPlot.plot(figsize=(15, 5), legend=True, title='Number of Projects and Successful Projects')
    leg = plt.legend(loc = 'upper right')
    plt.show()
    plt.figure();
    successfulRate.plot(figsize=(15, 4), legend=True, title='Success Rates')
    leg = plt.legend(loc = 'upper right')
    plt.show()
    # plt.figure();
    # backerNumMonthly.plot(figsize=(15, 4), legend=True, title='Backers')
    # plt.figure();
    # pledgedMonthly.plot(figsize=(15, 4), legend=True, title='Pledged Money')

    gamesNumMonthly = ksData[ksData['main_category'] == 'Games']['launched_month'].value_counts().rename('Number of games').sort_index()
    # gamesNumMonthly.plot( figsize=(15, 3), legend=True, title='Number of Games in Different Months')
    successfulGamesNumMonthly = ksData[(ksData['main_category'] == 'Games') & (ksData['state'] == 'successful')]['launched_month'].value_counts().rename('Number of successful Games').sort_index()
    # successfulGamesNumMonthly.plot(figsize=(15, 3), legend=True, title='Number of Games and Successful Games in Different Months')
    # plt.figure()
    gameSuccessfulRate = (successfulGamesNumMonthly/gamesNumMonthly).rename('Game Success Rate')
    plt.figure();
    gameSuccessfulRate.plot(figsize=(15, 3), title='Monthly Success Rate for Games')
    plt.show()


    categories = ksData['main_category'].unique()
    successfulRateAll = pd.Series()
    plt.figure();
    for category in ['Games', 'Technology', 'Design','Film & Video','Music','Publishing']: 
        numMonthly = ksData[ksData['main_category'] == category]['launched_month'].value_counts().rename('Number of' +str(category)).sort_index()
        successfulNumMonthly = ksData[(ksData['main_category'] == category) & (ksData['state'] == 'successful')]['launched_month'].value_counts().rename('Number of successful' + str(category)).sort_index()
        successfulRate = (successfulNumMonthly/numMonthly).rename(str(category) + ' Successful Rate')
    #     pd.concat([successfulRateAll, successfulRate], axis=1, join='inner')
    #     print(successfulRateAll)
        successfulRate.plot(figsize=(15, 7),legend=True, title='Monthly Success Rate for Six Categories')
        leg = plt.legend(loc = 'upper right')
    plt.show()


def regionPlot(ksData):
    # delete the rows that has no country 
    ksDataCountry = ksData
    ksDataCountry.drop(ksDataCountry[ksDataCountry['country'] == 'N,0"'].index , axis = 0, inplace = True) 
    ksDataCountry[ksDataCountry['country'] == 'N,0"'].index

    regionCount = ksData['country'].value_counts()
    countries = [x.lower() for x in list(regionCount.index)]
    # countriesDict = { i : 5 for i in listOfStr }
    countriesDict = {}
    for i in range(len(countries)):
        countriesDict[countries[i]] = regionCount[i]

    worldmap_chart = pygal.maps.world.World()
    worldmap_chart.title = str(regionCount.sum()) + ' Projects in Different Countries'
    # worldmap_chart.add('F countries', countriesDict)
    # worldmap_chart.add('M countries', ['ma', 'mc', 'md', 'me', 'mg',
    #                                    'mk', 'ml', 'mm', 'mn', 'mo',
    #                                    'mr', 'mt', 'mu', 'mv', 'mw',
    #                                    'mx', 'my', 'mz'])
    # worldmap_chart.add('U countries', ['ua', 'ug', 'us', 'uy', 'uz'])
    for i in range(len(countries)):
        worldmap_chart.add(regionCount.index[i] + ': '+str(round(regionCount[i]/regionCount.sum()*100,2)) + '%', {countries[i]:countriesDict[countries[i]]})

    display(SVG(worldmap_chart.render()))



    backerRegionCount = ksData.groupby('country')['backers'].sum().sort_values(ascending=False)
    countries = [x.lower() for x in list(backerRegionCount.index)]
    # countriesDict = { i : 5 for i in listOfStr }
    countriesDict = {}
    for i in range(len(countries)):
        countriesDict[countries[i]] = backerRegionCount[i]

    worldmap_chart = pygal.maps.world.World()
    worldmap_chart.title = str(backerRegionCount.sum()) + ' Backers in Different Countries'
    # worldmap_chart.add('F countries', countriesDict)
    # worldmap_chart.add('M countries', ['ma', 'mc', 'md', 'me', 'mg',
    #                                    'mk', 'ml', 'mm', 'mn', 'mo',
    #                                    'mr', 'mt', 'mu', 'mv', 'mw',
    #                                    'mx', 'my', 'mz'])
    # worldmap_chart.add('U countries', ['ua', 'ug', 'us', 'uy', 'uz'])
    for i in range(len(countries)):
        worldmap_chart.add(backerRegionCount.index[i] + ': '+str(round(backerRegionCount[i]/backerRegionCount.sum()*100,2)) + '%', {countries[i]:countriesDict[countries[i]]})

    display(SVG(worldmap_chart.render()))




