# Data Analysis for Kickstarter Projects 
Group 25: Kui Qian, Bowen Zhang, He Peng  

## Proposal

**Problem Statement:**

In this project, we try to give advice to college students about how to increase their chances of getting enough fundings for their creative projects on funding platforms like Kickstarter, by analysing the datasets of projects posted on Kickstarter.
 
**Real World Implications and Applications:**

Nowadays, more and more college students are trying to get their creative ideas take off. There are many funding platforms to help them achieve their goals, but students may not be able to raise enough money if they don’t plan it well. However, by analysing the existing data of successful and failed projects on Kickstarter, we could find the common features of successful projects. And there could also be some objective factors, like when and where the projects are posted, etc. The analysis results can help students make better choices when they try to raise money for their projects.
 
**Dataset:**

Kaggle dataset on Kickstarter projects (https://www.kaggle.com/kemical/kickstarter-projects).
The entire dataset includes two csv files, which collects data of more than 600,000 Kickstarter projects, providing various feature information such as category, category of campaign, fundraising goal, backers, and so on.
 
**Project Plan:**

1. Data cleaning: 1 week (Kui Qian)

2. Analysis of factors’ influence: 2 weeks (Kui Qian, Bowen Zhang, He Peng)

3. Data visualization: 1 week (Bowen Zhang, He Peng)
 
GitLab Repository:
https://gitlab.com/group25/data-analysis-for-kickstarter-projects.git


-----
## libraries We Use
Please install the libraries below before running the code:  

1. numpy
2. pandas
3. matplotlib
4. IPython.display
5. pygal
6. seaborn


